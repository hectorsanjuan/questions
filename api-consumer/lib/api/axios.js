const axios = require('axios');

module.exports = function makeRequest(endpoint) {
    const conn = axios.create({
        baseURL: endpoint
    });

    return conn
        .get('/tag')
        .then((response) => response.data)
        .catch((error) => {
            throw error.message;
        })
    ;
};
