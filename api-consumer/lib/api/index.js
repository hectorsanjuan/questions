// const makeRequest = require('./https');
// TODO: Toggle to use Axios lib for API call instead of HTTPS node module
const makeRequest = require('./axios');

module.exports = makeRequest;
