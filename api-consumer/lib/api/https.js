const https = require('https');

module.exports = function makeRequest(endpoint) {
    const url = `${endpoint}/tag`;

    return new Promise((resolve, reject) => fetchUrl(url, resolve, reject));
};

function fetchUrl(url, resolve, reject) {
    https
        .get(url, (response) => {
            const {statusCode, statusMessage} = response;
            const contentType = response.headers['content-type'];

            if (statusCode !== 200) {
                response.resume();

                return reject(statusMessage);
            }

            if (!/^application\/json/.test(contentType)) {
                response.resume();

                return reject(`Invalid content-type "${contentType}"`);
            }

            let responseBody = '';

            response.on('data', (chunk) => { responseBody += chunk });

            response.on('end', () => {
                try {
                    const tag = JSON.parse(responseBody);

                    resolve(tag);
                } catch (e) {
                    resolve(e.message);
                }
            });
        })
        .on('error', (err) => reject(err.message))
    ;
}
