const logger = require('./logger');
const api = require('./api');

const logfile = 'logs/requests.log';

module.exports = function makeLoggedRequest(endpoint) {
    const errorLog = logger(logfile);

    api(endpoint)
        .then((response) => {
            console.log('success', response);
            errorLog.info(response);
        })
        .catch((err) => {
            console.log('err', err);
            errorLog.error(err);
        })
    ;
};
