const {createLogger, format, transports} = require('winston');
const {combine, timestamp, prettyPrint} = format;

module.exports = function (filename) {
    return createLogger({
        level: 'info',
        format: combine(
            timestamp(),
            prettyPrint()
        ),
        transports: [
            new transports.File({filename})
        ]
    });
};
