// var fs = require('fs');
// var logStream = fs.createWriteStream('log.txt', {flags: 'a'});
// // use {flags: 'a'} to append and {flags: 'w'} to erase and write a new file
// logStream.write('Initial line...');
// logStream.end('this is the end line');
const fs = require('fs');

class Logger {
    filename;

    constructor(filename) {
        this.filename = filename;
        this.logStream = fs.createWriteStream(this.filename, {flags: 'a'});
    }

    log({level, message}) {
        const timestamp = new Date();

        this.logStream.write(`${timestamp.toISOString()} [${level}] ${message}`);
    }

    error(message) {
        this.log({level: 'error', message});
    }

    warn(message) {
        this.log({level: 'warn', message});
    }

    info(message) {
        this.log({level: 'info', message});
    }

    debug(message) {
        this.log({level: 'debug', message});
    }
}

module.exports = function (filename) {
    return new Logger(filename);
};
